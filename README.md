# Classmate Dynamic Wallpaper

Built using [https://github.com/mczachurski/wallpapper](https://github.com/mczachurski/wallpapper).

## To build
```bash
wallpapper -i wallpaper.json -o "Classmate Wallpaper.heic"
```

The wallpaper includes two images which are automatically activated based on macOS Light and Dark mode.


### Light Mode
![Light Mode Example](/white-on-blue.png)
### Dark Mode
![Dark Mode Example](/yellow-on-black.png)